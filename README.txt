
-- SUMMARY --

The Forum admin module adds the ability to add administrator email 
to every forum, the administrator will get email notifications about
every new topic or comment in the forum.


-- REQUIREMENTS --

This module requires the following modules:

 * forum (drupal core)
 * email (https://drupal.org/project/email)


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Fill administrator email on your forum edit page


-- CUSTOMIZATION --

* Site editor can edit the mail messages on the forum settings page, 
  on admin/structure/forum/settings and can use tokens on the message. 


MAINTAINERS
-----------

Current maintainers:
 * golds - https://www.drupal.org/user/2490548
